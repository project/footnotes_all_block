/* global Drupal, jQuery */

Drupal.behaviors.footnotesAllBlock = {
  /**
   * @param context
   */
  attach: function (context) {
    const $footnotes = jQuery('.footnotes .footnote, .footnotes .js-footnote-reference', context);
    const $footnotesAllBlock = jQuery('#footnotes_all_block', context);
    if ($footnotes.length > 0) {
      // Move every footnote to the block.
      $footnotes.appendTo($footnotesAllBlock);

      // Remove every footnote list except the one in the block.
      jQuery('.footnotes', context).not('#footnotes_all_block').remove();
    }
    else {
      // Remove empty wrapper if we have nothing to put in it.
      $footnotesAllBlock.remove();
    }
  }
};
